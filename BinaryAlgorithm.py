from math import inf
from random import randrange

# Implementation of the algorithm from Donald E. Knuth for optimal binary search trees.
# Used for testing!


def knuthAlgorithm(search_probs):
    # knuth's algorithm for optimal binary search trees
    found = {}
    def rec(start, end):
        if (start, end) in found:
            return found[(start, end)]
        if start > end:
            return 0, -2
        if start == end:
            found[(start, start)] = (search_probs[start], start)
            return search_probs[start], start
        cost_sum = sum([search_probs[i] for i in range(start, end+1)])
        min_cost = inf
        min_root = -2
        root_l = max(rec(start, end-1)[1], start)
        root_r = rec(start+1, end)[1]+1
        if root_r == -1:
            root_r = end+1
        for root in range(root_l, root_r):
            cost = cost_sum
            cost += rec(start, root-1)[0]
            cost += rec(root + 1, end)[0]
            if min_cost > cost:
                min_cost = cost
                min_root = root
        found[(start, end)] = (min_cost, min_root)
        return min_cost, min_root
    res, root = rec(0, len(search_probs)-1)
    return res, root


# Test algorithm against binary search tree algorithm. Used 
# as a quick (and inaccurate!) check for correctness
from TreeGenerators import RandomPath, RandomTree
from OPT2cutAlgorithms import OPT2CutSTTKnuth
test_algorithm = OPT2CutSTTKnuth # Has to return tuple (cost, tree) 
test_cases = 100
n = 100

for _ in range(test_cases):
    rp = RandomPath(n)
    props = [rp.getFreq(i+1) for i in range(n)]
    cost_OptSTT, tree = test_algorithm(rp)
    cost_real, root = knuthAlgorithm(props)
    # print(rp)
    if cost_real != cost_OptSTT:
        # print(tree)
        raise RuntimeError(f"cost not correct for frequencies {props}! knuth: {cost_real}, your: {cost_OptSTT}")

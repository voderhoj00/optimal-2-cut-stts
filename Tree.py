
# A tree class in python, that can perform needed operations 

import graphviz
from random import randrange, choice, shuffle
import heapq

class Tree:
    def __init__(self, name="tree", is_rooted=False):
        self._node_dict = {}
        self._freqs = {}
        # store data for efficiency:
        self._pathDirects = {} # stores results from GetPathDirection()
        self._cutNodes = {} # stores results from GetNodesFromCuts()
        self._cutNodesFreqs = {} # stores total sum of frequencies in GetNodesFromCuts()
        self._totalFrequencies = None
        self._name = name
        self._is_directed = is_rooted

    def addEdge(self, from_key, to_key, is_directed=False):
        # Adds an edge to the graph. If one of the vertices is not yet
        # in the graph, it will be added automatically
        if from_key not in self._node_dict:
            self.setNode(from_key, -1)
        if to_key not in self._node_dict:
            self.setNode(to_key, -1)
        self._node_dict[from_key].append(to_key)
        if not is_directed:
            self._node_dict[to_key].append(from_key)
        # Reset precalculations:
        self._cutNodes = {}
        self._pathDirects = {}
        self._totalFrequencies = None
    
    def GetTotalFrequencies(self):
        # returns total frequencies of three
        if self._totalFrequencies is None:
            self._totalFrequencies = sum([self.getFreq(i) for i in self._node_dict])
        return self._totalFrequencies

    def setNode(self, key, freq):
        # creates a node with a specific frequency
        if key not in self._node_dict:
            self._node_dict[key] = []
        # Sets the frequency for a specific node:
        self._freqs[key] = freq
    
    def getNeighs(self, key):
        return self._node_dict[key]

    def getFreq(self, key):
        # Gets the frequency for a specific node
        if key not in self._freqs:
            raise RuntimeError(f"key '{key}' has no frequency!")
        return self._freqs[key]

    def GetPathDirection(self, start, end):
        # Gets the next node on the path from start to end
        # Returns start, if start == end
        # Example:
        #          a
        #        /   \
        #       b     c
        #      / \   / \
        #     d  e  f   g
        #
        # GetPathDirection(b, f) = a
        # GetPathDirection(c, c) = c
        # GetPathDirection(b, a) = a

        # return, if already calculated:
        if ((start, end) in self._pathDirects):
            return self._pathDirects[(start, end)]
        for start_neigh in self._node_dict[start]:
            # DFS:
            queue = [start_neigh]
            seen = {start}
            while len(queue) != 0:
                new_node = queue.pop()
                self._pathDirects[(start, new_node)] = start_neigh
                for neigh in self._node_dict[new_node]:
                    if neigh not in seen:
                        queue.append(neigh)
                        seen.add(neigh)
        # For the case, where start = end
        self._pathDirects[(start, start)] = start
        return self._pathDirects[(start, end)]

    def GetPath(self, a, b):
        # Gets the path from a to b using GetPathDirection function
        path = [a]
        while a != b:
            a = self.GetPathDirection(a, b)
            path.append(a)
        return path

    def IsInComponent(self, a, cuts):
        # returns true, if a is in the subtree defined by cuts
        for (v, w) in cuts:
            if self.GetPathDirection(w, a) != v:
                return False
        return True 

    def GetNodesFromCuts(self, cuts):
        # Gets all nodes from subtree defined by cuts using DFS
        if len(cuts) == 0:
            return [i for i in self._node_dict]
        if len(cuts) == 1:
            if cuts[0] in self._cutNodes:
                return self._cutNodes[cuts[0]]
        result = []
        seen = {cuts[0][0]}
        for i in cuts:
            seen.add(i[1])
        queue = [cuts[0][0]]
        while len(queue) != 0:
            new_node = queue.pop()
            result.append(new_node)
            for neigh in self._node_dict[new_node]:
                if neigh not in seen:
                    queue.append(neigh)
                    seen.add(neigh)
        if len(cuts) == 1:
            self._cutNodes[cuts[0]] = result
            freqs_sum = sum([self.getFreq(i) for i in result])
            self._cutNodesFreqs[cuts[0]] = freqs_sum
        return result

    def GetNodeCountFromCuts(self, cuts):
        # Gets number of nodes in GetNodesFromCuts(self, cuts) efficiently
        cost_summed = sum([len(self.GetNodesFromCuts([(i[1], i[0])])) for i in cuts])
        return len(self._node_dict) - cost_summed

    def GetFrequenciesFromCuts(self, cuts):
        # Gets the total sum of frequencies in the subtree defined by cuts 
        freqs = 0
        for i in cuts:
            self.GetNodesFromCuts([(i[1], i[0])])
            freqs += self._cutNodesFreqs[(i[1], i[0])]
        return self.GetTotalFrequencies() - freqs
    
    def GetNumberOfLeaves(self):
        if self._is_directed:
            raise RuntimeError("tree must be undirected for this opration")
        # Gets the number of leaves in the tree
        return sum([1 for i in self._node_dict if len(self._node_dict[i])==1])

    """def GetConnectedComponents(self, node):
        # Gets the connected components of T \ {node}, if T is the tree
        if self._is_directed:
            raise RuntimeError("Get connected componentes only works on undirected trees!")
        components = []
        for neigh in self._node_dict[node]:
            C = Tree()
            nodes_of_c = self.GetNodesFromCuts([(v, node)])
            for v in :
                C.setNode(v, self.getFreq(v))
            for v in C._node_dict:
                for w in self._node_dict"""

    def _shiftTowards(self, cut, cut2):
        # shifts the cut, e.g. move it towards cut2 along the path
        v_1, w_1 = cut
        v_2, w_2 = cut2 
        v_1_prime = self.GetPathDirection(v_1, v_2)
        w_1_prime = w_1
        if v_1_prime != v_1:
            w_1_prime = v_1
        return v_1_prime, w_1_prime

    def _getRandomEdge(self):
        # only for testing. Can be ignored
        rand_vert = choice(list(self._node_dict))
        return (rand_vert, choice(self._node_dict[rand_vert]))
    
    def _getRand2Cut(self):
        # only for testing. Can be ignored
        e_1 = self._getRandomEdge()
        e_2 = self._getRandomEdge()
        while e_1 == e_2 or self.GetPathDirection(e_1[0], e_2[0]) == e_1[1] or self.GetPathDirection(e_2[0], e_1[0]) == e_2[1]:
            e_1 = self._getRandomEdge()
            e_2 = self._getRandomEdge()
        return e_1, e_2

    def _getMaxDegreeVert(self):
        # only for testing. Can be ignored
        as_t = [(len(self._node_dict[i]), i) for i in self._node_dict]
        return max(as_t)[1]

    def __len__(self):
        return len(self._node_dict)

    
    def __repr__(self):
        # prints the tree using gaphviz library and algoritm 'neato'. You can use
        # different engines for different looks. Possible engines:
        # dot, neato, twopi, circo
        penwidth = "2"
        if self._is_directed:
            g = graphviz.Digraph('G', filename=f'__graphviz/{self._name}.gv')
        else:
            g = graphviz.Digraph('G', filename=f'__graphviz/{self._name}.gv', engine="neato")
        seen_keys = set()
        g.node_attr['shape'] = 'circle'
        g.node_attr['penwidth'] = penwidth
        # directed graph version:
        for key in self._node_dict:
            seen_keys.add(key)
            for key_to in self._node_dict[key]:
                if key_to not in seen_keys or self._is_directed:
                    if key in self._node_dict[key_to]:
                        g.edge(str(key), str(key_to), penwidth=penwidth, dir='none')
                    else:
                        g.edge(str(key), str(key_to), penwidth=penwidth)
        # Set labels:
        for key in self._node_dict:
            if key in self._freqs:
                nLabel = f'<<font color="black">{str(key)}</font><br/><font color="darkgreen">{self._freqs[key]}</font>>'
                g.node(str(key), label=nLabel)
        
        """
        # Legend generated by openai codex:
        # Add legend to g, that explains that darkgreen letters are the frequencies and black letters the key:
        g.node('legend', label=f'<<b><font color="black">key</font></b><br/><b><font color="darkgreen">frequency</font></b>>', shape='plaintext')
        g.edge('legend', str(list(self._node_dict.keys())[0]), style='invis')
        
        # Add a border around the tree
        g.node('border', label='', shape='point', style='invis')
        g.edge('border', str(list(self._node_dict.keys())[0]), style='invis')
        """
        g.view()
        return "VizTree"
    
    def __eq__(self, other):
        # checks if two trees are equal
        return self._node_dict == other._node_dict

    def _getTotalCost(self):
        # calculates the total search cost(makes only sense if it is an STT)
        # should only be used for testing
        # find root:
        nodes = {i:0 for i in self._node_dict}
        for v in self._node_dict:
            for out in self._node_dict[v]:
                nodes[out] += 1
        root_node_tuple = min([(nodes[i], i) for i in self._node_dict])
        
        if root_node_tuple[0] != 0:
            raise RuntimeError("This tree is not rooted")
    
        def rec(curr_node, height=1):
            if len(self._node_dict[curr_node]) == 0:
                return self._freqs[curr_node]*height
            branch_cost = 0
            for neigh in self._node_dict[curr_node]:
                branch_cost += rec(neigh, height+1)
            return self._freqs[curr_node]*height + branch_cost
        
        return rec(root_node_tuple[1])
    
    
    def _getComponents(self, v):
        result = []
        # returns all subtrees of S \ {v} as new trees in a list
        for neig in self.getNeighs(v):
            subnodes = self.GetNodesFromCuts([(neig, v)])
            C = Tree(name=f"comp_{neig}")
            for subnode in subnodes:
                C.setNode(subnode, self.getFreq(subnode))
            for subnode in subnodes:
                for sub_neig in self._node_dict[subnode]:
                    if sub_neig != v and sub_neig not in C._node_dict[subnode]:
                        C.addEdge(sub_neig, subnode)
            result.append(C)
        return result
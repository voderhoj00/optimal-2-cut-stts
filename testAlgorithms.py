# Testing setup
import math
from Tree import Tree
from TreeGenerators import RandomTree, RandomStar, RandomStarPath, RandomPath, RandomBinaryTree
import time 
import matplotlib.pyplot as plt
from distutils.spawn import find_executable

def Test(algorithms, treefunc, min_n=10, max_n=100, stepsize=10, samples=4, plot=False, latexTable=False):
    # Basic test procedure: 
    timings = [[0]*len(algorithms) for _ in range(len(range(min_n, max_n+1, stepsize)))]
    curr_step = 1
    test_start_time = time.time()
    for n in range(min_n, max_n+1, stepsize):
        # print current progress:
        print(f"Step {curr_step}, n = {n}:", end="")
        sample_trees = [treefunc(n) for i in range(samples)]
        costs = [-1 for _ in range(samples)]
        for algo_ind, algo in enumerate(algorithms):
            algo = algorithms[algo_ind]
            start_time =  time.time()
            for sample_ind in range(len(sample_trees)):
                sample_tree = sample_trees[sample_ind]
                cost, tree = algo(sample_tree)
                # test for correctness:
                if costs[sample_ind] != -1 and costs[sample_ind] != cost:
                    raise RuntimeError(f"Algorithm {algo.__name__} gives not same result for following tree (gives cost {cost}, Algorithm {algorithms[algo_ind-1].__name__}: {costs[sample_ind]})\nNodes: {tree._node_dict}\nFrequencies: {tree._freqs}")
                costs[sample_ind] = cost
            exec_time = (time.time() - start_time)
            timings[curr_step-1][algo_ind] = exec_time/samples
        last_step_timings = ""
        for ind, exe_time in enumerate(timings[curr_step-1]):
            algorithm_name = algorithms[ind].__name__
            last_step_timings += f"|{algorithm_name}: {exe_time:.{3}f}"
        print(f" timings(average): {last_step_timings}|")
        curr_step += 1

    total_time = (time.time() - test_start_time)
    print(f"Total time taken: {total_time}")

    # The next lines are only used for visualization:

    if latexTable:
        # Generate latex table so that i can use it directly
        max_elem_float = max([max(l) for l in timings])
        max_elem = math.ceil(max_elem_float)
        with open("latexTable.txt", "w") as file:
            file.write("\\begin{tikzpicture} \n"+
                        "\\begin{axis}[xlabel={n}, ylabel={running time in seconds},"+
                        "legend style={at={(0.2,0.95)},anchor=north}, xtick=data, ymin=0, ytick={0,0.05,...,"+str(max_elem)+"}, ymajorgrids=true,"+
                        "grid style=dashed, width=15cm, height=8cm,]\n")
            file.write("\\foreach \i in {1,...,"+str(len(algorithms))+"} { \n"+
                       "    \\addplot table [x index=0, y index=\\i, row sep=\\\\] {\n")
            
            """
            algos = ""
            for algo_ind, algo in enumerate(algorithms):
                algos += f"|{algo.__name__}"
            file.write(f"algorithms: {algos}|\n")"""

            ranges = list(range(min_n, max_n+1, stepsize))
            for time_ind, times in enumerate(timings):
                file.write(f"       {ranges[time_ind]}")
                for algo_ind in range(len(algorithms)):
                    algo_time = times[algo_ind]
                    file.write(f" {algo_time:.{3}f}")
                file.write("\\\\\n")
            file.write("    };\n}\n")
            file.write("\legend{" + str([algo.__name__ for algo in algorithms])[1:-1].replace("'", "") + "}\n")
            file.write("\end{axis}\n\end{tikzpicture}")

    if plot:
        csfont = {'family' : 'serif', 'size'   : 12}
        styles = ['g--', 'r--', 'b--', 'y--', 'k--', 'c--', 'm--']
        algorithm_timings = [[] for i in range(len(algorithms))]
        for t in timings:
            for algo_ind in range(len(algorithms)):
                algorithm_timings[algo_ind].append(t[algo_ind])
        for algo_ind, algo_times in enumerate(algorithm_timings):
            algo_name = algorithms[algo_ind].__name__
            plt.plot(range(min_n, max_n+1, stepsize), algo_times, styles[algo_ind%len(styles)], label=algo_name)
        
        n_to_2 = [(i**2)*(max_elem_float/max_n**2) for i in range(min_n, max_n+1, stepsize)]
        algo_ind += 1
        plt.plot(range(min_n, max_n+1, stepsize), n_to_2, styles[algo_ind%len(styles)], label="$O(n^2)$")
        n_to_3 = [(i**3)*(max_elem_float/max_n**3) for i in range(min_n, max_n+1, stepsize)]
        algo_ind += 1
        plt.ylabel("time taken in seconds", **csfont)
        plt.xlabel("$n$", **csfont)
        plt.plot(range(min_n, max_n+1, stepsize), n_to_3, styles[algo_ind%len(styles)], label="$O(n^3)$")
        plt.legend()
        plt.show()
    return timings

from OPT2cutAlgorithms import OPT2CutSTTBasic, OPT2CutSTTBest, OPT2CutSTTKnuth, OPTKcutStt, ExactExponential
test_algorithms = [OPT2CutSTTBasic, OPT2CutSTTKnuth, OPT2CutSTTBest]
timings = Test(test_algorithms, RandomTree, plot=True, latexTable=True)
# In this module 3 algorithms for calculating optimal binary search trees are presented.
# These correspond to the pseudocode shown in the thesis

from math import inf
from Tree import Tree

def OPT2CutSTTBasic(tree):
    if tree._is_directed:
        raise RuntimeError("Input tree must be undirected!")
    # Version of OPT2cutTree without any improvements
    solved_cuts = {}

    def OptFromCuts(cuts):
        hash_val = tuple(cuts)
        if hash_val in solved_cuts:
            return solved_cuts[hash_val]
        
        A_size = tree.GetNodeCountFromCuts(cuts)
        if A_size == 1:
            A = tree.GetNodesFromCuts(cuts)
            solved_cuts[hash_val] = (tree.getFreq(A[0]), A[0])
            return (tree.getFreq(A[0]), A[0])
        elif len(cuts) == 2:
            c1, c2 = cuts
            allowedRoots = tree.GetPath(c1[0], c2[0])
        else:
            allowedRoots = tree.GetNodesFromCuts(cuts)
        
        min_cost = inf 
        min_root = None
        for r in allowedRoots:
            valid_neighs = [v for v in tree.getNeighs(r) if tree.IsInComponent(v, cuts)]
            this_cost = 0
            for neigh in valid_neighs:
                # build subtree cuts:
                this_cut = (neigh, r)
                subtree_cuts = [this_cut]
                for c in cuts:
                    if tree.IsInComponent(c[0], [this_cut]):
                        subtree_cuts.append(c)
                # calculate cost recursively:
                rec_cost, rec_root = OptFromCuts(subtree_cuts)
                this_cost += rec_cost
            if this_cost < min_cost:
                min_cost = this_cost
                min_root = r
        total_cost = min_cost+tree.GetFrequenciesFromCuts(cuts)
        solved_cuts[hash_val] = (total_cost, min_root)
        return total_cost, min_root
    
    #call with empty set of cuts:
    min_cost, _ = OptFromCuts([])

    return min_cost, BuildTreeFromCuts(tree, solved_cuts)

def OPT2CutSTTKnuth(tree):
    if tree._is_directed:
        raise RuntimeError("Input tree must be undirected!")
    # This is an adaption of OPT2CutSTTBasic with Knuth's trick generalization integrated
    solved_cuts = {}

    def OptFromCuts(cuts):
        hash_val = tuple(cuts)
        if hash_val in solved_cuts:
            return solved_cuts[hash_val]
        
        A_size = tree.GetNodeCountFromCuts(cuts)
        if A_size == 1:
            A = tree.GetNodesFromCuts(cuts)
            solved_cuts[hash_val] = (tree.getFreq(A[0]), A[0])
            return (tree.getFreq(A[0]), A[0])
        elif len(cuts) == 2:
            c1, c2 = cuts
            c1_prime = tree._shiftTowards(c1, c2)
            c2_prime = tree._shiftTowards(c2, c1)
            r_1 = c1[0]
            r_2 = c2[0]
            if c1_prime != c1:
                _, r_1 = OptFromCuts([c1_prime, c2])
            if c2_prime != c2:
                _, r_2 = OptFromCuts([c1, c2_prime])
            allowedRoots = tree.GetPath(r_1, r_2)
        else:
            allowedRoots = tree.GetNodesFromCuts(cuts)
        
        min_cost = inf 
        min_root = None
        for r in allowedRoots:
            valid_neighs = [v for v in tree.getNeighs(r) if tree.IsInComponent(v, cuts)]
            this_cost = 0
            for neigh in valid_neighs:
                # build subtree cuts:
                this_cut = (neigh, r)
                subtree_cuts = [this_cut]
                for c in cuts:
                    if tree.IsInComponent(c[0], [this_cut]):
                        subtree_cuts.append(c)
                # calculate cost recursively:
                rec_cost, _ = OptFromCuts(subtree_cuts)
                this_cost += rec_cost
            if this_cost < min_cost:
                min_cost = this_cost
                min_root = r
        total_cost = min_cost+tree.GetFrequenciesFromCuts(cuts)
        solved_cuts[hash_val] = (total_cost, min_root)
        return total_cost, min_root
    
    # call with empty set of cuts:
    min_cost, _ = OptFromCuts([])

    return min_cost, BuildTreeFromCuts(tree, solved_cuts)

def OPT2CutSTTBest(tree):
    if tree._is_directed:
        raise RuntimeError("Input tree must be undirected!")
    # This is a version with knuth's trick and improved cost check
    solved_cuts = {}
    solved_rootOpts = {}
    def OptFromCuts(cuts):
        hash_val = tuple(cuts)
        if hash_val in solved_cuts:
            return solved_cuts[hash_val]
        
        A_size = tree.GetNodeCountFromCuts(cuts)
        if A_size == 1:
            A = tree.GetNodesFromCuts(cuts)
            solved_cuts[hash_val] = (tree.getFreq(A[0]), A[0])
            return (tree.getFreq(A[0]), A[0])
        elif len(cuts) == 2:
            c1, c2 = cuts
            c1_prime = tree._shiftTowards(c1, c2)
            c2_prime = tree._shiftTowards(c2, c1)
            r_1 = c1[0]
            r_2 = c2[0]
            if c1_prime != c1:
                _, r_1 = OptFromCuts([c1_prime, c2])
            if c2_prime != c2:
                _, r_2 = OptFromCuts([c1, c2_prime])
            allowedRoots = tree.GetPath(r_1, r_2)
        else:
            allowedRoots = tree.GetNodesFromCuts(cuts)
        
        min_cost = inf 
        min_root = None
        for r in allowedRoots:
            # Neighbours of r, that point to a cut:
            NC = [tree.GetPathDirection(r, w) for (_, w) in cuts]

            # Calculate cost for every neighbour not pointing to cut,
            # but only if it was not already done previously
            if r not in solved_rootOpts:
                this_cost = 0
                for neigh in tree.getNeighs(r):
                    if neigh not in NC:
                        rec_cost, _ = OptFromCuts([(neigh, r)])
                        this_cost += rec_cost
                solved_rootOpts[r] = (this_cost, NC)
            
            precalc_cost, PrevNC = solved_rootOpts[r]

            # adding missing branches not part of NC:
            for x in PrevNC:
                if x not in NC:
                    precalc_cost += OptFromCuts([(x, r)])[0]

            # Now add remaining branches between cuts
            for c in cuts:
                cutNeigh = tree.GetPathDirection(r, c[1])
                if cutNeigh not in PrevNC:
                    precalc_cost -= OptFromCuts([(cutNeigh, r)])[0]
                if cutNeigh != c[1]:
                    precalc_cost += OptFromCuts([(cutNeigh, r), c])[0]
            
            if precalc_cost < min_cost:
                min_cost = precalc_cost
                min_root = r
        total_cost = min_cost+tree.GetFrequenciesFromCuts(cuts)
        solved_cuts[hash_val] = (total_cost, min_root)
        return total_cost, min_root
    
    # call with empty set of cuts:
    min_cost, _ = OptFromCuts([])

    return min_cost, BuildTreeFromCuts(tree, solved_cuts)


def BuildTreeFromCuts(tree, solved_cuts, sort_cuts=False):
    # This function calculates the optimal tree corresponding to
    # the calculations
    s_tree = Tree(name="STT", is_rooted=True)
    def rec_tree_build(cuts, parent):
        if sort_cuts:
            cuts.sort()
        hash_val = tuple(cuts)
        this_root = solved_cuts[hash_val][1]

        # add node to tree:
        s_tree.setNode(this_root, tree.getFreq(this_root))

        if parent is not None:
            s_tree.addEdge(parent, this_root, True)

        valid_neighs = [v for v in tree.getNeighs(this_root) if tree.IsInComponent(v, cuts)]

        for neigh in valid_neighs:
            this_cut = (neigh, this_root)
            subtree_cuts = [this_cut]
            for c in cuts:
                if tree.IsInComponent(c[0], [this_cut]):
                    subtree_cuts.append(c)
            rec_tree_build(subtree_cuts, this_root)
    rec_tree_build([], None)
    return s_tree

def OPTKcutStt(tree, k=3):
    # !!! Not tested much !!!
    # calculates the optimal k-cut STT
    if tree._is_directed:
        raise RuntimeError("Input tree must be undirected!")
    
    # Version of OPT2cutTree without any improvements
    solved_cuts = {}

    def OptFromCuts(cuts):
        if len(cuts) > k:
            return inf, None
        cuts.sort() # sort, so the hash value is unique
        hash_val = tuple(cuts)
        if hash_val in solved_cuts:
            return solved_cuts[hash_val]
        
        A_size = tree.GetNodeCountFromCuts(cuts)
        if A_size == 1:
            A = tree.GetNodesFromCuts(cuts)
            solved_cuts[hash_val] = (tree.getFreq(A[0]), A[0])
            return (tree.getFreq(A[0]), A[0])
        else:
            allowedRoots = tree.GetNodesFromCuts(cuts)
        
        min_cost = inf 
        min_root = None
        for r in allowedRoots:
            valid_neighs = [v for v in tree.getNeighs(r) if tree.IsInComponent(v, cuts)]
            this_cost = 0
            for neigh in valid_neighs:
                # build subtree cuts:
                this_cut = (neigh, r)
                subtree_cuts = [this_cut]
                for c in cuts:
                    if tree.IsInComponent(c[0], [this_cut]):
                        subtree_cuts.append(c)
                # calculate cost recursively:
                rec_cost, rec_root = OptFromCuts(subtree_cuts)
                this_cost += rec_cost
            if this_cost < min_cost:
                min_cost = this_cost
                min_root = r
        total_cost = min_cost+tree.GetFrequenciesFromCuts(cuts)
        solved_cuts[hash_val] = (total_cost, min_root)
        return total_cost, min_root
    
    #call with empty set of cuts:
    min_cost, _ = OptFromCuts([])
    return min_cost, BuildTreeFromCuts(tree, solved_cuts, True)

def ExactExponential(tree):
    # uses the fact, that an optimal L-cut tree is also an optimal STT
    # to calculate an optimal STT. !!! Not tested !!!
    L = tree.GetNumberOfLeaves()
    return OPTKcutStt(tree, L)


# Example:
"""from TreeGenerators import RandomTree
rand_tree = RandomTree(5)
print(rand_tree)
min_cost, actual_tree = OPT2CutSTTBest(rand_tree)
print(actual_tree._getTotalCost(), min_cost)
print(actual_tree)"""
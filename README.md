# Optimum 2-cut search trees on Trees

An experimental evaluation of the discussed algorithms for optimal 2-cut STT's.

This repository contains:

1. [A tree class](Tree.py) for the input datastructure for the algorithms.
2. An [implementation](BinaryAlgorithm.py) for the original optimal binary search tree algorithm from Knuth.
3. [Implementations](OPT2cutAlgorithms.py) of the various algorithms for finding optimal 2-cut STT's as discussed in the thesis
4. [Functions](TreeGenerators.py) for generating different kinds of trees for inputs.
5. A [function](testAlgorithms.py) for testing the performance of the algorithms.

## Requirements

To be able to visualize the trees, you need to have [graphviz](https://graphviz.org/download/) installed on you computer. Make sure to add the installation to the environment variables when installing. To use the library in in python, just use 
```
pip install graphviz
```

To plot the running times of the different functions, you have to install matplotlib with 
```
pip install matplotlib
```

## Data Structure
We have designed a data structure to efficiently store input trees and perform the necessary operations for our algorithms. The data structure provides the following essential functions:

1. GetPathDirection(self, a, b): Calculates the next node on the path from node $a$ to node $b$. This function stores its results in a private field called pathDirects for efficiency purposes.
2. GetPath(self, a, b): Returns the path from node $a$ to node $b$ using the GetPathDirection function.
3. IsInComponent(self, a, cuts): Determines whether node $a$ is inside the subtree defined by a given list of cuts. This function also uses the GetPathDirection function for efficiency.
4. GetNodesFromCuts(self, cuts): Calculates all the nodes inside the subtree defined by a given list of cuts. For efficiency, this function stores its results in a private variable called cutNodes, but only if the variable cuts contains one cut.
5. GetFrequenciesFromCuts(self, cuts): Computes the total sum of frequencies in the subtree defined by a list of cuts.

We have implemented the data structure in a dynamic way by storing previously calculated results internally. This approach eliminates the need for any precalculations manually.

## Algorithms
We have implemented the following five algorithms, all of which take a search space $S$ as input and return the optimal cost together with the optimal tree:

1. OPT2CutSTTBasic: Calculates the optimal 2-cut STT without using any performance improvements.
2. OPT2CutSTTKnuth: Implements the algorithm with Knuth's improvement.
3. OPT2CutSTTBest: Implements the algorithm with Knuth's improvement and better root-check.
4. OPTKcutSTT: Takes an additional parameter $k$ and calculates an optimal $k$-cut STT.
5. ExactExponential: Calculates an optimal STT on $S$. This algorithm first calculates the number of leaves $L$ of $S$ and calls the OPTKcutSTT algorithm with $L=k$.


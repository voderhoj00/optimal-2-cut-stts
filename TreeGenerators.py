# this modules stores functions for generating random trees 
from math import log2, ceil
from Tree import *

def TreeFromPrufercode(prufercode, key_gen = lambda: randrange(0, 100)):
    # This function creates the tree corresponding to a given prufercode
    # key_gen := function for generating random weights
    n = len(prufercode) + 2

    # count occurrences in prufer code:
    counts = {i:0 for i in range(1, n+1)}
    for i in prufercode:
        counts[i] += 1
    counts[n] += 1

    leaves = [i for i in counts if counts[i] == 0]
    heapq.heapify(leaves)
    tree = Tree()

    for i in prufercode:
        min_leaf = heapq.heappop(leaves)
        tree.addEdge(i, min_leaf)
        counts[i] -= 1
        if counts[i] == 0:
            heapq.heappush(leaves, i)
    last_leaf = heapq.heappop(leaves)
    tree.addEdge(n, last_leaf)

    # Set random frequencies:
    for i in range(1, n+1):
        tree.setNode(i, key_gen())
    return tree


def RandomTree(n, prufercode=None, key_gen = lambda: randrange(0, 100)):
    if n < 1:
        raise RuntimeError(f"Cannot generate tree with n < 1") 
    if n == 1:
        rt = Tree()
        rt.setNode(1, key_gen())
        return rt
    if prufercode is None:
        prufercode = [randrange(1, n+1) for i in range(n-2)]
    return TreeFromPrufercode(prufercode, key_gen)

def RandomPath(n):
    l = [i for i in range(2, n)]
    return RandomTree(n, prufercode=l)

def RandomStar(n):
    l = [1 for i in range(2, n)]
    return RandomTree(n, prufercode=l)

def RandomStarPath(n):
    # n = 3k + 2 must hold for some k in N, so k-2 must be divisible by 3
    k = int((n-2)/3)
    print(k)
    if k <= 0:
        raise RuntimeError("n > 4 must hold for generating a StarPath!")
    prufercode = []
    for i in range(k):
        prufercode.append(1)
    for i in range(2, k+3):
        prufercode.append(i)
    for i in range(k-1):
        prufercode.append(k+2)
    return TreeFromPrufercode(prufercode)

def RandomBinaryTree(n, key_gen = lambda: randrange(0, 100)):
    # generates a random binary tree
    rt = Tree()
    for i in range(1, n+1):
        rt.setNode(i, key_gen())
    for i in range(2, n+1):
        rt.addEdge(i//2, i)
    return rt
